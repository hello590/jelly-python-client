import time, os, json
from collections.abc import Iterable

PRIMITIVE_TYPES = ['int', 'str', 'bool', 'float', 'long', 'NoneType']

TYPE_KEY = '__type__'
EXIST_KEY = '__exist__'
STATE_KEY = '__state__'
RENDER_KEY = '__render__'
MOUNT_KEY = '__mount__'


def userfile (name, default=None):
  folder = os.path.normpath(os.path.expanduser('~/.jelly'))
  if not os.path.isdir(folder):
    os.makedirs(folder)
  filename = os.path.join(folder, name)
  if not os.path.isfile(filename):
    with open(filename, 'w') as f:
      if default is not None:
        f.write(default)
  return filename

JELLY_CONFIG_FILE = userfile('jelly.json', default=json.dumps({}))


class Uri:
  def __init__ (self, str):
    parts = str.split(':')
    self.type = parts[0]
    self.zone = parts[1]
    self.key = parts[2]
    self.parent = None
    parts = self.key.split('.')
    if len(parts) > 1:
      self.parent = make_uri(self.type, self.zone, '.'.join(parts[:-1]))

    self.module = parts[0]
    if len(parts) > 1:
      self.klass = parts[1]
    if len(parts) > 2:
      self.name = '.'.join(parts[2:])

  def __str__ (self):
    return "%s:%s:%s" % (self.type, self.zone, self.key)

  def __serialize__ (self, fn=None):
    return str(self)


def make_uri (type, zone, key):
  return type + ':' + zone + ':' + key

def make_object_uri (zone, key):
  return make_uri('object', zone, key)


class TYPE:
  LOG='LOG'
  EVENT='EVENT'
  OBJECT='OBJECT'

class LoggingLevel:
  STATUS = 60
  CRITICAL = 50
  ERROR = 40
  WARNING = 30
  INFO = 20
  DEBUG = 10
  NOTSET = 0

def log_level_name (level):
  if level == LoggingLevel.STATUS:
    return 'STATUS'
  elif level == LoggingLevel.CRITICAL:
    return 'CRITICAL'
  elif level == LoggingLevel.ERROR:
    return 'ERROR'
  elif level == LoggingLevel.WARNING:
    return 'WARNING'
  elif level == LoggingLevel.INFO:
    return 'INFO'
  elif level == LoggingLevel.NOTSET:
    return 'NOTSET'
  return ''

def get_level (name):
  for k, v in LoggingLevel.__dict__.items():
    if k == name:
      return v
  return LoggingLevel.NOTSET

class LogType:
  LOG=0
  EVENT=1
  ACTION=2
  OBJECT=3
  RPC=4

def timestamp ():
  return time.time()*1000.0

def is_primitive (value):
  return type(value).__name__ in PRIMITIVE_TYPES

def is_iterable (value):
  return isinstance(value, Iterable)

def is_list (value):
  return isinstance(value, list) or isinstance(value, tuple)

def is_dict (value):
  return isinstance(value, dict)

def is_module (value):
  return type(value).__name__ == 'module'

def is_builtin (value):
  return is_primitive(value) or is_iterable(value) or is_dict(value) or is_module(value)

def is_instance (value):
  return not is_builtin(value)

def flatten (alist, result=None):
  if result is None:
    result = []
  for item in alist:
    if is_list(item):
      flatten(item, result)
    elif is_primitive(item):
      result.append(item)
    else:
      raise Exception('cannot flatten type: ' + type(item).__name__)
  return result

from uuid import uuid4
def now ():
  return int(time.time() * 1000)

def uid ():
  return str(uuid4())

def function_name (fn):
  klass = fn.__self__.__class__.__name__
  return "%s.%s" % (klass, fn.__name__)

def getpath (obj, path):
  if isinstance(path, str):
    path = path.split('.')
  result = obj
  for p in path:
    if is_instance(result):
      result = getattr(result, p)
    elif is_dict(result):
      result = result[p]
    elif is_iterable(result):
      result = result[int(p)]
    else:
      raise ValueError('%s does not exist' % p)
  return result

__ID__ = '__id__'
def object_key (obj):
  return "%s:%s" % (obj.__class__.__name__, object_id(obj))

def object_id (obj):
  if hasattr(obj, __ID__):
    result = getattr(obj, __ID__)
    if callable(result):
      return str(result())
    return str(result)
  return str(id(obj))
  

class Serializer:
  def __init__ (self):
    pass

  def serialize (self, value, full=False):
    return self._serialize(value, [], full=full)
    
  def _serialize (self, value, path, **kwargs):
    result = None
    full = kwargs.get('full', False)
    if is_primitive(value):
      result = value
    elif is_dict(value):
      result = {k: self._serialize(v, path + [k], **kwargs) for k, v in value.items()}
    elif is_iterable(value):
      result = [self._serialize(v, path + [str(i)], **kwargs) for i, v in enumerate(value)]
    elif hasattr(value, '__serialize__'):
      result = value.__serialize__(self.serialize)
    elif is_instance(value):
      state = {}
      shape = None
      methods = []
      
      if hasattr(value, STATE_KEY):
        state = getattr(value, STATE_KEY)()
      if hasattr(value, RENDER_KEY):
        methods.append(RENDER_KEY)
        try:
          shape = self._serialize(getattr(value, RENDER_KEY)(), path)
        except Exception as err:
          print(err)
      
      if hasattr(value, MOUNT_KEY):
        methods.append(MOUNT_KEY)
      result = {}
      result.update(**{
        'class': type(value).__name__,
        'shape': shape,
        'methods': methods,
        'id': state.get('id'), 'ts': timestamp(),
        #'str': str(value),
        'state': None if not full else self._serialize(state, path, **kwargs)
      })
    else:
      result = {TYPE_KEY: 'unknown'}
    return result
