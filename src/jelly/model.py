from collections import namedtuple
from typing import Mapping
from . import ui



class Property:
  def __init__(self, default=None) -> None:
    self.key = None
    self.default = default

  def __set__ (self, inst, value):
    node = inst.__dict__['__jelly_node__']
    node.update(**{self.key: value})

  def __get__ (self, inst, cls=None):
    node = inst.__dict__['__jelly_node__']
    return node.attributes.get(self.key, self.default)

class ref (Property): 
  def __init__ (self, name):
    super().__init__()
    self.name = name
  
  def __get__ (self, inst, cls=None):
    node = inst.__dict__['__jelly_node__']
    # TODO: wait connecting until actually has be used
    #  Might not be able to find_node when getting, but maybe thats okay
    ref_node = node.api.find_node(self.name)
    node.api.connect(ref_node.uri, node.uri, self.key)
    return NodeProxy(ref_node)

class NodeProxy:
  def __init__ (self, node):
    self.__node = node
    
  def __getattr__ (self, name):
    if name in ['__node']:
      return object.__getattribute__(self, name)
    try:
      return getattr(self.__node.handler(), name)
    except AttributeError as err:
      raise AttributeError("%s has no attribute %s" % (self.__node.handler(), name))
  
  def __serialize__ (self, x):
    # TODO: might need a separate serializer for when wanting a element
    node = self.__node
    return ui.element(node.tag_name)(value=node.uri)

  def __call__ (self, key, **kw):
    # TODO: node.child should ensure unique key
    child_node = self.__node.child(key)
    child_node.update(**kw)
    return NodeProxy(child_node)


class string(Property):
  pass

class property(Property):
  pass

class Model:
  def __init__ (self, module, key):
    self.module = module
    self.key = key
    self.handler = None

  def to_dict (self):
    return dict(selector=self.key, type='MODEL')

  def __call__ (self, cls):
    self.handler = cls
    self.__cls = cls
    for k, v in self.__cls.__dict__.items():
      if isinstance(v, Property):
        v.key = k
    Registry.register(self.key, self)
    return cls


class Component(Model):
  
  def to_dict (self):
    return dict(selector=self.key, type='COMPONENT')

  def __mount__ (self, node):
    inst = self.handler()
    inst.__dict__['__jelly_node__'] = node
    return inst


class Service(Model):

  def to_dict (self):
    return dict(selector=self.key, type='SERVICE')

  def __provide__ (self, node):
    inst = self.handler()
    inst.__dict__['__jelly_node__'] = node
    if hasattr(inst, '__register__'):
      inst.__register__(node)
    
    return inst
    
class Module:
  def __init__ (self, key):
    self.key = key

  def component (self, key: str):
    return Component(self.key, self.key + '.' + key)
  
  def service (self, key):
    return Service(self.key, self.key + '.' + key)

class _Registry:
  def __init__ (self):
    self.classes: Mapping[str, Model] = {}
    #self.items: Mapping[str, UriModel] = {}

  def register (self, key, cls):
    self.classes[key] = cls

  def register_uri (self, key, hnd):
    pass
    #self.items[]

  def get (self, key):
    return self.classes[key]

  def to_dict (self):
    return {k: v.to_dict() for k, v in self.classes.items()}

Registry = _Registry()


