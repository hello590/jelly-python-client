

"""
class ActionType:
  object_added='object_added'
  object_removed='object_removed'
  function_called='function_called'

class Profiler:
  def __init__ (self, session, root=None):
    self.session = session
    self.root = root or os.path.abspath('.')
    self.stack = []
    self.group_id = uid()
    self._objects = {}
    
  def run (self, fn):
    sys.setprofile(self._on_frame)
    try:
      return fn()
    finally:
      sys.setprofile(None)
      
  def _add_object (self, obj):
    try:
      obj_key = object_key(obj)
      if obj_key not in self._objects:
        self._objects[obj_key] = weakref.ref(obj)
        weakref.finalize(obj, self._on_object_deleted, obj_key)
        self.publish(dict(
          action=ActionType.object_added, key=obj_key
        ))
    except Exception as err:
      LOG.error('failed to register object: %s %s', obj, type(obj))
  def _on_object_deleted (self, obj_key):
    self.publish(dict(
      action=ActionType.object_removed, key=obj_key
    ))
  def _function_called (self, source, target):
    self.publish(dict(
      action=ActionType.function_called, source=source, target=target
    ))

  def publish (self, log):
    log.update(
      type=LogType.ACTION, levelno=LoggingLevel.INFO, ts=timestamp(),
      group=self.group_id
    )
    #self.session.publish_log(log)
    #print(log)

  def _on_frame (self, frame, event, arg):
    if not frame:
      return
    info = inspect.getframeinfo(frame)
    if info.filename.startswith(self.root):
      is_call = event == 'call' # or event == 'c_call')
      is_return = event == 'return' # or event == 'c_return')
      is_builtin = info.function.startswith('__')
      pf = ProfileFrame(frame, info)
      
      if pf.instance and info.function == '__init__' and is_return:
        pass
        #self.session.add_object(pf.instance)
        #self._add_object(pf.instance)
      
      prev_pfs = [s for s in self.stack[::-1] if s.instance] #self.stack[-1]
      if pf.instance and prev_pfs and is_call and len(self.stack) > 0: # and not is_builtin:
        prev_pf = prev_pfs[0]
        source_key = object_key(prev_pf.instance)
        target_key = object_key(pf.instance)
        if source_key != target_key:
          self._function_called(source_key, target_key)

      if is_call:
        self.stack.append(pf)
      elif is_return and len(self.stack):
        self.stack.pop()
      else:
        return


class ProfileFrame:
  def __init__ (self, frame, info):
    self._frame = frame
    self._info = info
    self._arg_info = inspect.getargvalues(frame)
    self.instance = None
    self.is_method = len(self._arg_info.args) and self._arg_info.args[0] == 'self'
    if self.is_method:
      self.instance = self._arg_info.locals['self']
      objkey = object_key(self.instance)
      self.key = "%s:%s" % (objkey, self._info.function)
    else:
      self.key = self._info.function

  def __str__ (self):
    cls = ''
    if self._instance:
      cls = self._instance.__class__.__name__
    inst = '' if not self._instance else id(self._instance)
    args = ', '.join(self._arg_info.args)
    return "%s.%s(%s)" % (cls, self._info.function, args)

  def __hash__(self) -> int:
    return id(self._frame)
"""