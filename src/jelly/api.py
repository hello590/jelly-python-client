import logging, time

from collections import defaultdict
from typing import List, Mapping
from .util import Uri, make_object_uri, now
from .client import JellyClient
from .ui import RenderContext, element
from .model import Registry
from .store import JellyStore
from .transport import MQTT
from .auth import FirebaseAuth

LOG = logging.getLogger(__name__)
div = element('div')

class Edge:
  def __init__ (self, from_uri: str, to_uri:str, attribute:str):
    self.from_uri = from_uri
    self.to_uri = to_uri
    self.attribute = attribute

class Node:
  def __init__ (self, api: 'JellyAPI', uri: str):
    self.api = api
    self.uri = uri
    self.meta = dict(
      create_time=now(), update_time=now()
    )
    self.tag_name = Uri(uri).key.split('.')[0]
    self.attributes = {}
    self.observers = []
    self._state = 'UNKNOWN'
    self._render_context = RenderContext()
    self._handler = None
    print(' -', self.uri)
    
  def __spec__ (self):
    return dict(uri=self.uri, ts=now(), meta=self.meta, attributes={})
    
  def __state__ (self):
    return dict(uri=self.uri, attributes=self.attributes, 
      shape=self._render_context.to_dict())

  def __repr__ (self):
    return "<node #%s>" % self.uri

  def __str__ (self):
    return repr(self)

  def set_state (self, state):
    self.attributes = state.get('attributes', {})
    
  def child (self, name):
    uri = Uri(self.uri)
    child_uri = make_object_uri(uri.zone, '%s.%s' % (uri.key, name))
    return self.api.node(child_uri)

  def auto_create_handler (self):
    if self._handler:
      return self._handler
    parent = self.parent()
    if parent and parent.mountable():
      self._handler = parent.mount(self)
    self.api.on_spec_update(self.uri)
    return self._handler

  def handler (self):
    return self.auto_create_handler()

  def parent (self):
    uri = Uri(self.uri)
    if uri.parent:
      return self.api.node(uri.parent)

  def mountable (self):
    handler = self.auto_create_handler()
    return hasattr(handler, '__mount__')

  def mount (self, child_node):
    handler = self.auto_create_handler()
    inst = getattr(handler, '__mount__')(child_node)
    return inst

  def is_ready (self):
    return self._state == 'READY'
  
  def trigger (self, id, evt):
    ctx = dict(start_time=time.time())
    self._render_context.trigger(id, evt)
    ctx['reason'] = 'event triggered'
    self.refresh(**ctx)

  def attribute (self, key):
    return Attribute(self, key)
    #if not key in self.attributes:
    #  self.attributes[key] = Attribute(self, key)
    #return self.attributes[key]

  def set_handler (self, handler):
    if hasattr(handler, '__provide__'):
      handler = getattr(handler, '__provide__')(self)
    self._handler = handler
    # TODO: should properly not be here, might need some refactor
    self.api.on_spec_update(self.uri)
    return self

  def update (self, **attrs):
    self.attributes.update(**attrs)
    self._state = 'READY'
    self.meta['update_time'] = now()
    return self.refresh(reason='updated %s' % (attrs))

  def refresh (self, **ctx):
    handler = self.auto_create_handler()
    print('evaluate', self.uri, ctx.get('reason', 'unknown'))

    if handler and hasattr(handler, '__render__'):
      try:
        render = getattr(handler, '__render__')
        self._render_context.reset()
        render(self._render_context)
      except Exception as err:
        LOG.exception(err)
        self._render_context.template(
          div('__error__')('ERROR: ' + str(err))
        )
        
    if 'start_time' in ctx:
      ctx['total_time'] = time.time() - ctx['start_time']
    self.api.emit(self.uri, **ctx)
    return self

class Attribute:
  def __init__ (self, node: Node, key):
    self._node = node
    self._key = key

  def _ready (self):
    return self._node.is_ready()

  def __getattr__ (self, name):
    if name.startswith('_'):
      return super().__getattribute__(name)
    return self._node.attributes[self._key][name]


class JellyModule:
  def __init__ (self, api: 'JellyAPI', id: str):
    self.id = id
    self.api = api
    self.client = JellyClient(id, api.auth)
    self.zone = self.client.client_id # TODO: remove this?
    self.store = JellyStore('%s.db' % id)
    self.nodes: Mapping[str, Node] = {}
    self.edges: Mapping[str, Edge] = {}
    self._subscribers = defaultdict(set)
    self.open()

  def open (self):
    print('open network', self.id)
    objects = self.store.all()
    for uri, obj in objects.items():
      self.node(uri).set_state(obj)
    self.introduce()

  def introduce (self, recv=None):
    payload = dict(spec=self.__spec__())
    if recv is None:
      self.client.broadcast('__introduce__', payload)
    else:
      self.client.send_to_client(recv, '__introduce__', payload)

  def consume (self):
    for msg in self.client.messages():
      print('->', msg)
      try:
        self._on_message(msg.zone, msg.type, **msg.payload)
      except Exception as err:
        LOG.exception(err)

  
  def _on_message (self, sender, type, **kw):
    args = ', '.join([k + '=' + str(v) for k, v in kw.items()])
    print(sender + ':', type + '(' + args + ')')
    if type == '__connect__':
      self._subscribers[kw['uri']].add(sender)
      self.node(kw['uri']).refresh(reason='%s connected' % sender)
      #self.emit(kw['uri']) # TODO: only need to emit to "sender"
    elif type == '__trigger__':
      self.node(kw['uri']).trigger(kw['id'], kw['event'])
    elif type == '__introduce__':
      self.introduce(sender)
      #self.client.send_to_client(sender, '__introduce__', self.spec())
    elif type == '__disconnect__':
      self.disconnect_client(sender)
    else:
      raise RuntimeError('unknown method: ' + type)
    
  def __spec__ (self):
    return {k: v.__spec__() for k, v in self.nodes.items()}

  def on_spec_update (self, uri):
    print('   + spec', uri)
    node = self.node(uri)
    self.client.broadcast('__spec__', node.__spec__())

  def _list_edges (self, from_uri) -> List[Edge]:
    return [e for e in self.edges.values() if e.from_uri == from_uri]

  def disconnect_client (self, client_id):
    print('disconnect client', client_id)
    for k, v in self._subscribers.items():
      if client_id in v:
        v.remove(client_id)
      if len(v) == 0:
        self.remove_uri(k)
  
  def remove_uri (self, uri):
    # removes node and all its connections
    print('remove uri', uri)
    new_edges = {}
    for k, edge in self.edges.items():
      if edge.from_uri == uri or edge.to_uri == uri:
        pass
      else:
        new_edges[k] = edge
    self.edges = new_edges

  def register (self, key, model):
    uri = make_object_uri(self.zone, key)
    return self.node(uri).set_handler(model)

  # TODO: remove this, use connect or something instead
  #  use a better selection-strategy than "first in loop"
  #  have a index where we put all nodes found by all in network
  def find_node (self, name):
    for node in self.nodes.values():
      if Uri(node.uri).key == name:
        return node
    raise ValueError('"%s" not found' % name)

  def node (self, uri) -> Node:
    Uri(uri) # TODO:.. 
    if not uri in self.nodes:
      self.nodes[uri] = Node(self, uri)
    return self.nodes[uri]

  def connect (self, from_uri, to_uri, attribute):
    k = from_uri + ':' + to_uri + ':' + attribute
    if k in self.edges:
      return
    self.edges[k] = Edge(from_uri, to_uri, attribute)
    print('connect', from_uri, '->', to_uri + '.' + attribute)

  def update (self, uri, **attributes):
    self.node(uri).update(**attributes)
    
  def emit (self, uri, **ctx):
    zones = set()
    state = self.node(uri).__state__()
    self.store.put(uri, state)

    for edge in self._list_edges(uri):
      zone = Uri(edge.to_uri).zone
      if zone == self.zone:
        self.node(edge.to_uri).update(**{edge.attribute: state['attributes']})
      else:
        zones.add(zone)
    
    for zone in zones:
      pass
      
    if 'start_time' in ctx:
      ctx['total_time'] = (time.time() - ctx['start_time']) * 1000.0

    state['ctx'] = ctx
    receivers = self._subscribers[uri]
    if not receivers:
      return
    print('emit', uri, 'to', receivers)
    for client_id in self._subscribers[uri]:
      self.client.send_object(client_id, state)
  
  def _on_receive_object (self, uri, state):
    self.node(uri).update(**state['attributes'])


class JellyAPI:
  def __init__ (self):
    self.auth = FirebaseAuth()
    self.registry = Registry
    self.modules: Mapping[str, JellyModule] = {}

  def login (self, username, password):
    self.auth.login(username, password)

  def load (self):
    print('loading jelly...')
    for key, model in self.registry.classes.items():
      self.module(model.module).register(key, model)

  def module (self, id):
    if not id in self.modules:
      self.modules[id] = JellyModule(self, id)
    return self.modules[id]

  def run (self):
    self.load()
    while 1:
      networks = list(self.modules.values())
      for network in networks:
        network.consume()
      time.sleep(0.001)

  
