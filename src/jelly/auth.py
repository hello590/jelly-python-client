import requests, json
from .util import JELLY_CONFIG_FILE

FB_TOKEN = "AIzaSyA8ZD0UanZMks-_0H5f3-WXnFQhnv6Onqg"


class FirebaseAuth:
  def __init__ (self):
    self.session = requests.Session()
    self.session.headers = {"content-type": "application/json"}

    self._project_id = 'web-jelly'
    self._modules_collection = 'projects'

    self.config_file = JELLY_CONFIG_FILE
    self.config = {}
    self.keys = {}
    self.username = None
    self.password = None
    self.host = "{host}/projects/{projectId}/databases/{databaseId}".format(
        host="https://firestore.googleapis.com/v1beta1",
        projectId=self._project_id, databaseId="(default)"
    )

  def login (self, username, password):
    self.username = username
    self.password = password
    self._load()

  def update (self, **values):
    new_config = self.config.copy()
    new_config.update(**values)
    with open(self.config_file, 'w') as f:
      f.write(json.dumps(new_config))
    self._load()

  def _load (self):
    config = {}
    with open(self.config_file, 'r') as f:
      config = json.loads(f.read())
    
    if config and 'token' in config and config.get('user') == self.username and 'uid' in config:
      self.session.headers.update({
        'Authorization': "Bearer " + config['token']
      })
    self.config = config
    
  def _do_signout (self):
    AUTH = 'Authorization'
    self.session.headers[AUTH] = None

  def _do_signin (self):
    details = dict(email=self.username, password=self.password, returnSecureToken=True)
    url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key={}'.format(FB_TOKEN)
    r = self.session.post(url, data=json.dumps(details)) # .encode("utf-8")
    r.raise_for_status()
    auth = r.json()
    self.update(uid=auth['localId'], token=auth['idToken'], user=self.username)
    
  def _request (self, method, url, params=None, retry=0):
    resp = self.session.request(method, url)
    if resp.status_code in [401,403] and not retry:
      self._do_signout()
      self._do_signin()
      return self._request(method, url, params=params, retry=retry+1)
    resp.raise_for_status()
    return resp.json()

  def fetch_keys (self, module_id):
    if module_id in self.keys:
      return self.keys[module_id]
    url = "{host}/documents/{collection}/{doc}/keys".format(
      host=self.host, collection=self._modules_collection, doc=module_id
    )
    data = self._request('GET', url)
    keys = []
    for doc in data['documents']:
      key = doc['fields']['secret']['stringValue']
      network = 'v1/network/%s' % key
      keys.append(dict(
        key=key
      ))

    result = keys[0]['key']
    self.keys[module_id] = result    
    return result
