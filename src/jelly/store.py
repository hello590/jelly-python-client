import json
import sqlite3 as sl
from .util import userfile

class JellyStore:
  def __init__ (self, name):
    self.filename = userfile(name)
    self.conn = sl.connect(self.filename)
    self.create()

  def create (self):
    with self.conn:
      result = self.conn.execute("""
        SELECT name FROM sqlite_master WHERE type='table' AND name='NODE';
      """)
      for row in result:
        if row[0] == 'NODE':
          return
      self.conn.execute("""
          CREATE TABLE NODE (
              id VARCHAR(250) NOT NULL PRIMARY KEY,
              data TEXT
          );
      """)

  def clear (self):
    with self.conn:
      self.conn.execute("DELETE FROM NODE")

  def get (self, key):
    with self.conn:
      data = self.conn.execute("SELECT * FROM NODE WHERE id = '%s'" % key)
      for row in data:
        return json.loads(row[1])

  def all (self):
    with self.conn:
      data = self.conn.execute("SELECT * FROM NODE")
      result = {}
      for row in data:
        result[row[0]] = json.loads(row[1])
      return result

  def put (self, key, data):
    sql = 'INSERT INTO NODE (id, data) values(?, ?)'
    data = [
        (key, json.dumps(data))
    ]
    with self.conn:
      self.conn.executemany(sql, data)


if __name__ == '__main__':
  import pprint
  store = JellyStore('test')
  store.clear()
  key = 'cellular-spear.css-1023202-100'
  store.put(key, dict(
    report_time=100, reporting=False, registered=True
  ))
  data = store.get(key)
  pprint.pprint(data)

  not_exist = store.get('does-not-exist')
  print('NULL:', not_exist)

  all_ = store.all()
  pprint.pprint(all_)
  store.clear()