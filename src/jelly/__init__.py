import logging, sys, time, os

from .client import JellyClient
from .api import JellyAPI
from .model import Module

from watchdog.observers import Observer


LOG = logging.getLogger(__name__)
LOG.propagate = False


module = Module

global __jelly__
__jelly__ = None
def init (*path, **config):
  global __jelly__
  __jelly__ = None
  route = []
  for p in path:
    if not isinstance(p, str):
      raise ValueError('path arguments must be string')
    for r in p.split('.'):
      if r:
        route.append(r)
  
  scope = '.'.join(route[:3])
  if not __jelly__:
    
    client = JellyClient(scope)
    client.configure(**config)
    __jelly__ = JellyAPI(client, scope)

  return __jelly__

def instance ():
  global __jelly__
  if not __jelly__:
    raise ValueError('jelly has not been instanitated')
  return __jelly__

def run (username, password, auto_reload=False):
  global __jelly__
  if __jelly__ is None:
    __jelly__ = JellyAPI()
  
  __jelly__.login(username, password)
  class Handler:
    def dispatch(self, event):
      print('-- restarting --')
      os.execv(sys.executable, ['python'] + sys.argv)
  
  if auto_reload:
    observer = Observer()
    path = os.path.abspath('.')
    print(' - watching path for changes:', path)
    event_handler = Handler()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    if __jelly__:
      try:
        __jelly__.run()
      except KeyboardInterrupt:
        print('keyboard interupt')
        observer.stop()
        sys.exit(0)
    print('run exit, will auto restart in 10 seconds')
    observer.stop()
    time.sleep(10)
    os.execv(sys.executable, ['python'] + sys.argv)
  else:
    __jelly__.run()