import sys, logging, os, requests, json, pprint, time
from collections import namedtuple

from .transport import MQTT
from .util import JELLY_CONFIG_FILE, uid, Serializer
from .auth import FirebaseAuth

LOG = logging.getLogger(__name__)




class JellyClient:
  def __init__ (self, network_id, auth: FirebaseAuth, version='v1', host='ws.web-jelly.com', port=1883):
    self.version = version
    self.id = network_id
    self.network_key = None
    self.client_id = uid()
    self.config = dict(
      host=host, port=port
    )
    self.routes = {}
    self.transport = MQTT(self.config['host'], self.config['port'], self.client_id)
    self.auth = auth
    self._serializer = Serializer()
    self.open()
    
  def open (self):
    self.network_key = self.auth.fetch_keys(self.id)
    broadcast_topic = 'v1/network/%s' % self.network_key
    self.transport.subscribe(
      broadcast_topic, 'v1/inbox/client/%s' % self.client_id
    )
    self.transport.will = dict(
      topic=broadcast_topic, 
      payload=json.dumps(dict(type='__disconnect__', sender=self.client_id, payload=dict()))
    )
    self.transport.connect()

  def serialize (self, x):
    return self._serializer.serialize(x)

  def route (self, zone, client_id):
    self.routes[zone] = client_id

  def kill (self):
    self.transport.wait_for_publish()
    os._exit(0)
  
  def subscribe_broadcast (self):
    return self.transport.subscribe("%s/%s" % (self.version, 'broadcast'))

  def configure (self, **kw):
    pass
    #self.config.update(**kw)
    #self.transport = MQTT(self.config['host'], self.config['port'], self.client_id)
    #app_path = os.path.abspath(self.config.get('app', '.'))
    #sys.path.append(app_path)

  def send_object (self, client_id, state):
    self.send_to_client(client_id, '__object__', state)

  def send (self, zone, type, data):
    client = self.routes[zone]
    self.send_to_client(client, type, data)
    
  def send_to_client (self, client, type, data):
    msg = dict(
      sender=self.client_id, type=type, payload=data
    )
    msg = self.serialize(msg)
    topic = 'v1/inbox/client/' + client
    self.transport.publish(topic, msg)

  def broadcast (self, type, payload):
    topic = 'v1/network/%s' % self.network_key
    msg = dict(
      sender=self.client_id, type=type, payload=payload
    )
    msg = self.serialize(msg)
    self.transport.publish(topic, msg)

  def is_known_client (self, client_id):
    return True # TODO: implement

  def messages (self):
    
    for req in self.transport.messages():
      try:
        if req.payload['sender'] != self.client_id:
          yield self.handle_message(req)
      except Exception as err:
        LOG.exception(err)

  def handle_message (self, req):
    sender_id = req.payload['sender']
    if not self.is_known_client(sender_id):
      raise RuntimeError('unknown sender')
    msg = Message(req.payload['sender'], req.payload['type'], req.payload['payload'])
    return msg

  def login (self, username, password):
    while 1:
      try:
        return self.auth.login(username, password)
      except Exception as err:
        LOG.error('failed to login: %s', err)
      LOG.info('will retry login in 10 seconds..')
      time.sleep(10)

Message = namedtuple('Message', field_names=['zone', 'type', 'payload'])

