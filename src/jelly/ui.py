import os, json
from .util import Uri, uid


class LocalStorage:
  def __init__ (self):
    self.filename = os.path.abspath(os.path.expanduser('~/.jelly-local-storage.json'))
    self.data = {}
    
  def reload (self):
    try:
      with open(self.filename, 'r') as f:
        self.data = json.loads(f.read())
    except:
      print('failed to load local storage')
    

  def get (self, key):
    self.reload()
    return self.data.get(key)

  def set (self, key, data):
    self.reload()
    self.data[key] = data
    try:
      with open(self.filename, 'w') as f:
        f.write(json.dumps(self.data))
    except:
      print('failed to save local storage')

localStorage = LocalStorage()


class Element:
  def __init__ (self, tag_name, attributes=None, styles=None, children=None):
    self.tag_name = tag_name
    self.attributes = attributes or {}
    self.styles = styles or {}
    self.children = children or []
    self.listeners = {} # defaultdict(list)

  def click (self, event):
    return self.on(click=event)

  def on (self, **kw):
    for k, v in kw.items():
      #if not isinstance(v, Event):
      #  v = Event(v)
      self.listeners[v._id] = dict(type=k, value=v)
    return self

  def style (self, **kw):
    self.styles.update(**kw)
    return self

  def mixin (self, elm: 'Element'):
    self.attributes.update(**elm.attributes)
    self.styles.update(**elm.styles)
    self.listeners.update(**elm.listeners)
    return self

  def __str__ (self):
    children = ''.join(map(str, self.children))
    props = ' '.join([k + '="' + str(v) + '"' for k, v in self.attributes.items()])
    sep = ' ' if props else ''
    return "<%s%s%s>%s</%s>" % (
      self.tag_name, sep, props, children, self.tag_name
    )

  def copy (self):
    return Element(self.tag_name, styles=self.styles, attributes=self.attributes, children=self.children)
  
  def id (self, str):
    return self.update(id=str)

  def update (self, **attrs):
    attrs = {k: v for k, v in attrs.items() if v is not None}
    self.attributes.update(**attrs)
    return self

  def __call__ (self, *children):
    self.children = children
    return self

  def __serialize__ (self, serializer):
    props = {k: serializer(p) for (k, p) in self.attributes.items()}
    props['style'] = {k: serializer(p) for (k, p) in self.styles.items()}
    listeners = {k: serializer(v) for k, v in self.listeners.items()}
    return dict(
      type=self.tag_name, attributes=props, 
      listeners=listeners, #key=self.key,
      children=[serializer(c) for c in self.children]
    )


class ElementType(Element):
  def __call__ (self, *items, **kw):
    elm = self.copy()
    for item in items:
      if isinstance(item, str):
        elm = elm.id(item)
      elif isinstance(item, Element):
        elm = elm.mixin(item)
    elm = elm.update(**kw)
    return elm

def element (tag_name='') -> ElementType:
  return ElementType(tag_name)


class Event:
  def __init__ (self, handler):
    self._id = uid()
    self._handler = handler

  def __serialize__ (self, x):
    return dict(type='event', id=self._id)

  def __call__ (self, evt):
    return self._handler(evt)


class Javascript:
  def __init__ (self, api):
    self.context = api
    self.variables = {}

  def var (self, name):
    self.variables[name] = JSVariable(name)
    return self.variables[name]

  def to_dict (self):
    return dict(
      variables={k: v.to_dict() for k, v in self.variables.items()}
    )

class JSVariable:
  def __init__ (self, name):
    self.name = name

  def to_dict (self):
    return dict(name=self.name)

  def __serialize__ (self, x):
    return self.to_dict()

class RenderContext:
  def __init__ (self):
    self._events = {}
    self.js = Javascript(self)
    self.reset()

  def to_dict (self):
    return dict(js=self.js.to_dict(), template=self._template)

  def reset (self):
    self._stylesheets = []
    self._template = None
    #self._events = {} # TODO: hack for web sometimes using old events

  def stylesheet (self, name, content=''):
    self._stylesheets.append(dict(type='style', id=name))

  def trigger (self, key, event):
    print(self._events)
    self._events[key](event)

  def template (self, *elements):
    self._template = [] + self._stylesheets + list(elements)

  def event (self, handler):
    evt = Event(handler)
    self._events[evt._id] = evt
    return evt
